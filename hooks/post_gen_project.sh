#!/bin/bash

# Rename files extension based on syntax flavour
{% if cookiecutter.use_typescript == "y" %}
mv src/index.jsx src/index.tsx
mv src/components/App/app.jsx src/components/App/app.tsx
mv src/components/App/app.test.js src/components/App/app.test.ts
{% else %}
rm tsconfig.json
{% endif %}

# Rename file based on style flavour
{% if cookiecutter.style == 'scss' or cookiecutter.style == 'sass' %}
mv src/components/App/app.css src/components/App/app.{{ cookiecutter.style }}
{% endif %}

# Add/rename necessary file
mv .env.example .env

# Add in files what we have for now
git init
git add .
yarn install
yarn format
yarn lint:fix
git add .

# Create standard branches and link to remote
{% if cookiecutter.upload_repo == "y" %}
git remote add origin git@bitbucket.org:wspdigitaluk/{{ cookiecutter.repo_name }}.git
git commit -m 'Initial setup commit'
git push -f --set-upstream origin master
git checkout -b test
git push -f --set-upstream origin test
git checkout master
{% endif %}

# Check package dependencies version
yarn outdated
read -p 'Upgrade all packages to latest version (y|n)?' yn
case $yn in
    [Yy]*) yarn upgrade;;
    *) ;;
esac
echo 'Project generation completed. Run `cd {{ cookiecutter.repo_name }} && docker-compose up` to bring up your project if you happy with the setup.'
