#!/usr/bin/env sh

set -eu

# Install docker-compose via pip
pip3 install --no-cache-dir docker-compose cookiecutter virtualenv pre-commit
docker-compose -v
