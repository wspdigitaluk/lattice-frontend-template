#!/bin/sh
rm -R base-project

orig_dir=$(pwd)
tmp_dir=$(mktemp -d -t cookie-XXXXXXXXXX)

cd "$tmp_dir"
cookiecutter "$orig_dir" --no-input
cd base-project
git init
git remote add origin git@bitbucket.org:wspdigitaluk/backend_postgres_output.git
git add .
git commit -m init-commit
git push -f --set-upstream origin master
