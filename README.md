
# Lattice Frontend Template

This is front end project template for Lattice which will create a Rancher deployable with React.

## Dependencies

The dependenciess required are:

1. Python 3.7 and pip
2. cookiecutter (PyPI)
3. docker-compose and docker
4. NodeJS and yarn

## Creating a Project

In order to use the project simply open a terminal in the location you'd like the service's repo to be stored and type:

```
#: cookiecutter git@bitbucket.org:wspdigitaluk/lattice-frontend-template.git      
```

There is no need to clone the base project manually unless developing the template. You will be prompted with the 
following options, sensible defaults will be generated based on the repo name. For example if the repo name is 
`base-frontend` you will get the following defaults:

```
repo_name [base-frontend]:
upload_repo [y]:
project_name [Base Frontend]:
project_slug [base_frontend]:
domain_name [base-frontend]:
feeds_channel [#base_frontend_feeds]:
use_typescript [y]: 
Select style:
1 - css
2 - sass
3 - scss
Choose from 1, 2, 3 [1]:
Select component:
1 - function
2 - class
Choose from 1, 2 [1]:
use_authentication_routes [y]:
```

More detail about the parameters are:

- repo_name: This is the name of the repo in wspdigital's Bitbucket. This will be given a default value based on the
   project name but you can change this if you like
- upload_repo: Enter 'y' to upload the repo to bitbucket. Any other selection will not push the created project to
   bitbucket for you. Selecting 'y' will __force__ push the new project to the remote and create `test`
   and `stag` branches based off the `master` branch
   **NOTE:** Make sure the blank repo has been created in Bitbucket first before proceed. 
- project_name: This is the human readable name of the project. Stick to Title Case and spaces
- project_slug: This is the name of the project, of which it will generally be recognized programming wise
- domain_name: This name is used normally for Rancher config files
- feeds_channel: The slack channel for rancher webhooks (Make sure you create this beforehand in repo setting)
- use_typescript: Option to use Typescript as main coding language. Otherwise, Javascript (JSX) is used if other value is entered
- style: Option to choose main style sheet for the project. Default is pure CSS
- component: Option to generate initial React component with either functional or class-based component. Default is functional.
- use_authentication_routes: Option to use Authentication Service V3 routes as part of Nginx. If selected, the project will load 500 error page on the browser if relevant authentication env variables are not yet set.

Your project will be ready once the script has been run. `cd` to designated repo to start developing

## Developing the Template

This template has a CI pipeline to ensure that the template generated is a functioning service. The pipeline runs the
file `test-creation-ci.sh` Which runs the cookiecutter template with the default values, apart from `upload_repo` which
is set to false by the `test-config.yaml` (Otherwise our CI pipeline will try and upload a repo to bitbucket). This script
generates a project and in the `post_gen_project.sh` it will do a cleanup based on selection.

When developing the template it is important to remember that you must modify the code in `{{cookiecutter.repo_name}}`
and __not__ in the output of the generation, as re-running cookiecutter will override any changes you have made, this
can make it difficult to test the project and development is easier if you follow a TDD approach and ensure the test
pipeline passes on the generated app.

Testing Rancher/Nginx is much harder and the workflow involves running the ./scripts/test-creation.sh on a repo that
is hooked up to rancher and seeing the resulted deployed project

# How do I use my template service?

This is covered in the README.md inside the generated repo


# Troubleshooting

First confirm you have all the dependencies and try again if not

## Script failed due to port in use

Its best to make sure you don't have any running containers. Free up the port by terminating any service currently occupying it.
