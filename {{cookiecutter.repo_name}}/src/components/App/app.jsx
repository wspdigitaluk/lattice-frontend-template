import React from 'react'
import './app.{{ cookiecutter.style }}'

{% if cookiecutter.use_typescript == 'y' %}
interface AppProps {
    name: string
}
{% if cookiecutter.component == 'class' %}
interface AppState {
    showGreeting: boolean
}
{% endif %}
{% endif %}

{% if cookiecutter.component == 'class' %}
export default class App extends React.Component{% if cookiecutter.use_typescript == 'y' %}<AppProps, AppState>{% endif %} {
    /**
     * Dummy class-based component starter
     */
    constructor(props{% if cookiecutter.use_typescript == 'y' %}: AppProps{% endif %}) {
        super(props)
        this.state = { showGreeting: true }
    }

    render(){% if cookiecutter.use_typescript == 'y' %}: JSX.Element{% endif %} {
        return (
            <div className="app" onClick={() => this.setState({ showGreeting: !this.state.showGreeting })}>
                { this.state.showGreeting && <h1>Welcome to {this.props.name}!</h1> }
            </div>
        )
    }
}
{% else %}
export default function App(props{% if cookiecutter.use_typescript == 'y' %}: AppProps{% endif %}){% if cookiecutter.use_typescript == 'y' %}: JSX.Element{% endif %} {
    /**
     * Dummy functional component starter
     */
    const [showGreeting, setShowGreeting] = React.useState(true)

    return (
        <div className="app" onClick={() => setShowGreeting(!showGreeting)}>
            {showGreeting && <h1>Welcome to {props.name}!</h1>}
        </div>
    )
}
{% endif %}