import React from 'react'
import { render } from 'react-dom'
import App from './components/app/app'
import * as Sentry from '@sentry/react'

if (process.env.SENTRY_DSN !== undefined) {
    Sentry.init({
        dsn: process.env.SENTRY_DSN,
        environment: process.env.ENVIRONMENT,
    })
}

render(<App name="{{ cookiecutter.project_name }}" />, document.getElementById('root'))
