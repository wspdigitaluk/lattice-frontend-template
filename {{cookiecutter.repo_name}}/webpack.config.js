const HtmlWebPackPlugin = require('html-webpack-plugin')
const DotenvPlugin = require('dotenv-webpack')
const webpack = require('webpack')
const path = require('path')
const babelTestFiles = /\.(js|jsx{% if cookiecutter.use_typescript == 'y' %}|ts|tsx{% endif %})$/
const allowedExtensions = ['.js', '.jsx'{% if cookiecutter.use_typescript == 'y' %}, '.ts', '.tsx'{% endif %}]

{% if cookiecutter.style == 'scss' or cookiecutter.style == 'sass' %}
const styleTestFiles = /\.(css|{{ cookiecutter.style }})$/
{% else %}
const styleTestFiles = /\.(css)$/
{% endif %}

module.exports = {
    context: __dirname,
    devtool: 'source-map',
    devServer: {
        disableHostCheck: true,
        host: '0.0.0.0',
        inline: true,
        port: 80,
        contentBase: path.join(__dirname, 'dist'),
        hot: true,
        public: 'localhost',
        watchOptions: {
            poll: true,
        },
    },
    module: {
        rules: [
            {
                test: babelTestFiles,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
            {
                test: styleTestFiles,
                use: [
                    'style-loader',
                    'css-loader',
                    {% if cookiecutter.style == 'scss' or cookiecutter.style == 'sass' %}
                    'sass-loader',
                    {% endif %}
                ],
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader',
                    },
                ],
            },
        ],
    },
    resolve: {
        extensions: allowedExtensions,
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: './src/index.html',
            filename: './index.html',
        }),
        new DotenvPlugin(),
        new webpack.EnvironmentPlugin(['SENTRY_DSN'{% if cookiecutter.use_authentication_routes == 'y' %}, 'AUTH_SERVICE_HOST'{% endif %}]),
    ],
}
