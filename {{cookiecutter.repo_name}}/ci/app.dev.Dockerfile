FROM quay.io/wspdigitalukteam/npmbuild:bionic

WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install
