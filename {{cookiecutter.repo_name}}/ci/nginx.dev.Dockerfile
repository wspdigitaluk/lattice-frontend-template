FROM nginx:1.17
RUN apt update && apt install --no-install-recommends -y nginx-extras && rm -rf /var/lib/apt/lists/* && rm -rf /etc/nginx/conf.d/*
COPY nginx-config/nginx.root /etc/nginx/nginx.conf
COPY nginx-config/nginx.conf /etc/nginx/sites-enabled/default
COPY nginx-config/nginx-location-dev.conf /app/nginx/location.conf
{% if cookiecutter.use_authentication_routes == 'y' %}
RUN sed -i '/\[::\]:80/ a \ \ \ \ listen 443 ssl;\n \
   ssl_certificate /usr/share/certs/localhost.apps.wsp-lattice.io.pem;\n \
   ssl_certificate_key /usr/share/certs/localhost.apps.wsp-lattice.io-key.pem;\n \
   ssl_protocols TLSv1.2;' /etc/nginx/sites-enabled/default
{% endif %}
CMD ["nginx", "-g", "daemon off;"]
