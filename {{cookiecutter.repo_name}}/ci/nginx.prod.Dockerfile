FROM nginx:1.17
RUN apt update && apt install --no-install-recommends -y nginx-extras && rm -rf /var/lib/apt/lists/* && rm -rf /etc/nginx/conf.d/*
COPY nginx-config/nginx.root /etc/nginx/nginx.conf
COPY nginx-config/nginx.conf /etc/nginx/sites-enabled/default
COPY nginx-config/nginx-location-prod.conf /app/nginx/location.conf
COPY ci/nginx-entry.sh /etc/nginx-entry.sh
