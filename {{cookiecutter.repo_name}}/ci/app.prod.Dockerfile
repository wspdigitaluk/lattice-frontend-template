FROM quay.io/wspdigitalukteam/npmbuild:bionic

WORKDIR /app
COPY package.json yarn.lock webpack.config.js .babelrc ./
RUN yarn install --ignore-optional && yarn cache clean
COPY src ./src/
