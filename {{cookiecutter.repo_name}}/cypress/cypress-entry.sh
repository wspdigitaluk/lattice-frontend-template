#!/bin/bash -e

echo wait for app to be ready...
timeout 60 bash -c 'until cat < /dev/null > /dev/tcp/app/80; do sleep 1; done' &>/dev/null

curl -s -o /dev/null http://app/

./node_modules/.bin/cypress run "$@"